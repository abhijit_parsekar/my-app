import React, {Component} from 'react'
import MyPagedTable from "./MyPagedTable"

class App extends Component {
    render() {
        return (
            <MyPagedTable/>
        )
    }
}

export default App